//create array
//two ways
//use array literal []
/*const array1 = ['eat', 'sleep'];
console.log(array1);

//use the new keyword
const array2 = new Array('pray', 'play');
console.log(array2);
*/
//empty array
/*const myList = [];

//array of numbers
const numArray = [2, 3, 4, 5];

//array of strings
const stringArray = ['eat', 'work', 'pray', 'play'];

//array of mixed data but not recommended
const newData = ['work', 1, true];

//array with obejects, function
const newData1 = [
	{task1: 'exercise'},
	[1, 2, 3],
	function hello(){
		console.log('Hi I am array')
	}
];
console.log(newData1);
*/
//mini activity
/*create an array with 7 items; all strings. 
	-list seven of the places you want to visit someday
	-log the first item in the console
	-log all the items in the console

	*/
/*	let placeToVisit = [
	'Japan',
	'South Korea',
	'Israel',
	'New Zealand',
	'Switzerland',
	'Batanes',
	'Siargao'
	];*/
/*console.log(placeToVisit[0]);
console.log(placeToVisit);
console.log(placeToVisit[placeToVisit.length-1]);
console.log(placeToVisit.length);

for(let i = 0; i < placeToVisit.length; i++){
	console.log(placeToVisit[i]);
}*/

//Array manipulation
// push() - add element at the end of an array
/*let dailyActivities = ['eat', 'work', 'pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);
//unshift() - add element at the beginning of an array
dailyActivities.unshift('sleep');
console.log(dailyActivities);
//change the string in the array
dailyActivities[2] = 'sing';
console.log(dailyActivities);
//note: pwede mag add sa unahan at sa dulo ng array pero hindi pwedeng mag add sa gitna. change lang
dailyActivities[6] = 'dance';
console.log(dailyActivities);

//re-assign the values/items in an array:
placeToVisit[3] = "Giza Sphinx";
console.log(placeToVisit);
console.log(placeToVisit[5]);

placeToVisit[5] = 'Turkey';
console.log(placeToVisit);
console.log(placeToVisit[5]);

//mini activity
/* 
	re assign the values for the first and last item in the array
	-re assign it with your home town and your highschool
	log the array in the console
	log the first and last items in the console
	*/
	/*placeToVisit[0] = "Nueva Vizcaya";
	placeToVisit[placeToVisit.length-1] = 'NVSU';
	console.log(placeToVisit);
	console.log(placeToVisit[0]);
	console.log(placeToVisit[placeToVisit.length-1]);
*/
//adding items in an array without using methods
/*let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
array[array.length-1] = 'Aerith Gainsboro';
console.log(array);
array[array.length] = 'Vincent Valentine';
console.log(array);
*/
/*
	array methods
	manipulate array with pre-determined JS Functions
	mutators- array methods that usually change the original array
/*	*/
	/*let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
//without method - changes the original array
array1[array.length] = 'Francisco';
console.log(array1);// andres will be changed to francisco

//.push() - allows to add an element at the end of the array
array1.push('Andres');
console.log(array1);

//.unshift() - allows to add an element at the beginning of the array
array1.unshift('Simon');
console.log(array1);

// .pop() - allows to delete or remove the last item at the end of the array
// is also able to return the item we removed
array1.pop();
console.log(array1);
console.log(array1.pop());
console.log(array1);

let removedItem = array1.pop();
console.log(array1);
console.log(removedItem);
*/
//.shift()- removes the element at the beginning of an array
/*let removedItemShift = array1.shift();
console.log(array1);
console.log(removedItemShift);
*/
/*mini activity
	Remove the first item of array1 and log array1 in the console
	Delete the last item of array1 and log array1 in the console
	Add 'George' at the start of array1 and log in the console
	Add 'Michael' at the end of array1 and log in the console
	Use array methods
	*/
/*	let removedFirstItem = array1.shift();
	console.log(removedFirstItem);

	let removedLastItem = array1.pop();
	console.log(removedLastItem);

	array1.unshift('George');
	console.log(array1);

	array1.push('Michael');
	console.log(array1);

//.sort()- by default, it will allow us to sort our items in ascending order.
array1.sort();
console.log(array1);

let numArray = [3, 2, 1, 6, 7, 9];
numArray.sort();
console.log(numArray);

let numArray2 = [32, 400, 459, 2, 9, 5, 50, 90];
numArray2.sort((a,b)=> b-a);
console.log(numArray2);// converts all items into strings and then arrange the items accordingly as if they are words*/

//ascending sort per number's value
/*numArray2.sort(function(a,b){
	return a-b;
})
console.log(numArray2);

//descending sort per number's value
numArray2.sort(function(a,b){
	return b-a;
})*/
/*console.log(numArray2);

let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
arrayStr.sort(function(a,b){
	return b-a;
})
console.log(arrayStr);*/

//.reverse() - reverse the order of items
/*arrayStr.sort();
console.log(arrayStr);
arrayStr.sort().reverse();
console.log(arrayStr);*/

/*let beatles = ['George', 'John', 'Paul', 'Ringo'];
let lakersPlayers = [
		'Lebron', 
		'Davis', 
		'Westbrook', 
		'Kobe', 
		'Shaq'
];*/
//splice()- allows us to remove and add elements from a given index
//syntax: array.splice(startingIndex,numberofItemstobeDeleted, elementstoAdd)
/*lakersPlayers.splice(0,0, 'Caruso');
console.log(lakersPlayers);
lakersPlayers.splice(0,1);
console.log(lakersPlayers);
lakersPlayers.splice(0,3);
console.log(lakersPlayers);//Kobe, Shaq
lakersPlayers.splice(1,1);
console.log(lakersPlayers);//Kobe
lakersPlayers.splice(1,0, 'Gasol', 'Fisher');
console.log(lakersPlayers);
*/
//non-mutators 
/*
	methods that will not change the original array
	slice()- gets a portion of the original array and return a new array with the items selected from the original
	syntax: slice(startIndex, endIndex)
*/
/*let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
console.log(computerBrands);

let newBrands = computerBrands.slice(1,3);
console.log(computerBrands);
console.log(newBrands);*/

let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];
console.log(fonts);

let newFontSet = fonts.slice(1,3);
console.log(newFontSet);

/*let newFontSet = fonts.slice(1,5);
console.log(newFontSet);

newFontSet = fonts.slice(2);
console.log(newFontSet);*/

/*
	mini-activity
	given a set of data, use slice to copy the last two items in the array
	save the sliced portion of the array into a new variable - microsoft
	use slice to copy the 3rd and 4th item in the array
	saved the sliced portion into new variable: nintendo
	display in console.log
*/

let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];
console.log(videoGame);
let microsoft = videoGame.slice(3);
console.log(microsoft);

let nintendo = videoGame.slice(2,4);
console.log(nintendo);

//.toString() - convert the array into a single value as a string but each item will be separated by a comma
//syntax: array.toString()

let sentence = ['I', 'like', 'JavaScript', '.', 'It', 'is', 'fun', '.'];
let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

//.join() - converts the array into a single value as a string but separator can be specified
// syntax: array.join(separator)
let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
let sentenceString2 = sentence2.join(' ');
console.log(sentenceString2);
let sentenceString3 = sentence2.join('');
console.log(sentenceString3);

//mini activity - oct 8
/*
	given a set of characters:
	- form the name, "Martin" as single string
	- form the name, "Miguel" as single string
	use the methods we discussed so far

	save "Martin" and "Miguel" to a variable:
	-name1 and name2
	log both variable on the console
*/
let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];
let nameA = charArr.slice(5,11);
let nameB = charArr.slice(13,19);
let name1 = nameA.join('');
let name2 = nameB.join('');
/*shorthand
	let name1 = charArr.slice(5,11).join('');
	let name2 = charArr.slice(13,19).join('');
	console.log(name1, name2);
*/

console.log(nameA);
console.log(nameB);
console.log(name1);
console.log(name2);

//.concat() - it combines 2 or more arrays without affecting the original
//syntax: array.concat(array1, array2)

let tasksFriday = ['drink HTML', 'eat JavaScript'];
let tasksSaturday = ['inhale CSS', 'breath Bootstrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday, tasksSunday);
console.log(weekendTasks);

//Accessors
//methods that allow us to access our array
//indexOf() - finds the index of the given element/item when it is first found from the left

/*let batch131 = [
		'Paolo',
		'Jamir',
		'Jed',
		'Ronel',
		'Rom',
		'Jayson'
];
console.log(batch131.indexOf('Jed'));//2
console.log(batch131.indexOf('Rom'));//4

//lastIndexOf() - finds the index of the given element when it is last found from the right

console.log(batch131.lastIndexOf('Jamir'));//1
console.log(batch131.lastIndexOf('Jayson'));//5
console.log(batch131.lastIndexOf('Kim'));//-1
*/
/*
	mini activity
	given a set of brands with some entries repeated
		create a function which can display the index of the brand that was input the first time it was found in the array.

		create a function which can display the index of the brand that was input the last time it was found in the array.
*/
let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];
function firstIndex(item) {
	console.log(carBrands.indexOf(item));
}
firstIndex('BMW')

function lastIndex(item) {
	console.log(carBrands.lastIndexOf(item));
}
lastIndex('BMW');

//iterator methods
// these methods iterate over the items in an array much like loop. it also allows not only iterate over items but also additional instruction

let avengers = [
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spider-man',
		'Iron Man',
		'Captain America'
];

//forEach()
//similar to for loop but it is used on arrays. it will allow us to iterate over each time in an array and even add instruction per iteratio
//Anonymous function within forEach will be received each and every item in an array

avengers.forEach(function(avenger){
	console.log(avenger);
});

let marvelHeroes = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHeroes.forEach(function(hero){
	//iterate over all the items in MarvelHeroes array and let them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		//add an if-else wherein Cyclops and Deadpool is not allowed to join
		avengers.push(hero);
	}
});
console.log(avengers);

//map() - similar to forEach however it returns new array

let number = [25, 50, 30, 10, 5];
let mappedNumbers = number.map(function(number) {
	console.log(number);
	return number * 5;
});
console.log(mappedNumbers);
//creates new array

//every()
	//iterates over all the items and checks if all the elements passes a given condition
let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age) {
	console.log(age);
	return age >= 18;
})
console.log(checkAllAdult);// 25, 30, 15 false
//pag false na mag sstop siya

//some()
	// iterate over all the items check if even at least one of items in the array passes the condition. same as every(), it will return boolean
let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;// 75, 80 true
});
console.log(checkForPassing);
//pag true na mag sstop siya

//filter() - creates new array that contains elements which pass a fiven condition
let numbersArray2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArray2.filter(function(number){
	return number % 5 === 0;
});
console.log(divisibleBy5);//500,120,60,30

//find()- iterate over all items in our array but will only returns the item that will satisfy the fiven condition
let registeredUsername = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

let foundUser = registeredUsername.find(function(username){
	console.log(username);
	return username === 'mikeyTheKing2000';
});
console.log(foundUser);

//.includes() - return a boolean true if it finds a matching item in the array
// case sensitive

let registeredEmails = [
		'johnnyPhoenix@gmail.com',
		'michaelKing@gmail.com',
		'pedro_himself@yahoo.com',
		'sheJonesSmithAgmail.com'
];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');
console.log(doesEmailExist);//true

/*
mini activity
create 2 functions 
first - is able to find specified or the username input in our registeredUsernames array. Display the result in the console

second - is able to find a specified email already exist in the registeredEmails array
	- if there is an email found, show an alert:
	"Email Already Exist."
	- if there is no email found, show an alert:
	"Email is available, proceed to registration."

	You may use any of the three methods 
*/
let findUser = registeredUsername.find(function(username){
	return username === 'superPhoenix';
});
console.log(findUser);


function emailExist(emailadd){
	let confirmEmal = registeredEmails.includes(emailadd);
	if(emailadd == true){
		alert("Email Already Exist")
	} else {
		alert("Email is available, proceed to registration.");
	}
}
emailExist('jollibeeMcdo@yahoo.com')




